//getValueFromCrontab("python /home/pi/.scripts/pinControl.py",function(res){
//	console.log(res);
//})

var amountCodes={
	0:"minutes",
	1:"hours",
	2:"days",
	3:"months",
	4:"weekdays",
}

function makeCronString(val,a){
	var cronStrings={
		"min":'*/'+val+' * * * *',
		"hr":'0 */'+val+' * * *',
		"d":'0 0 */'+val+' * *',
		"m":'0 0 0 */'+val+' *',
		"wd":'0 0 0 0 */'+val,
	}
	return cronStrings[a];
}

exports.getValueFromCrontab = function (key,callback){
	var bash = require('child_process').spawn,
		readValue=bash('crontab',['-l']);
	readValue.stdout.on('data',function(data){
		str=data.toString();
		arr=str.split('\n');
		for (i=arr.length-1;i>=0;i--){
			if(arr[i].indexOf(key)>-1){
				kArr=arr[i].split(' ');
				lArr=kArr.slice(0,5);
				for (j=0;j<lArr.length;j++){
					if (lArr[j].indexOf('/')>-1){
						result=lArr[j].split('/')[1]+' '+amountCodes[j];
						break;
					}
				}
				callback(result);
				break;
			}
		}
	})
}

exports.editCrontab = function(val,amount,callback){
	console.log("editCt entered")
	var cronstring=makeCronString(val,amount);
	console.log(cronstring);
	var remCron = require('child_process').spawn,
		readValue=remCron('crontab',['-r']);
	var cmdline='(crontab -l 2>/dev/null; echo "'+cronstring+' python /home/pi/.scripts/pinControl.py ")| crontab -';
	var createCron = require('child_process').exec(cmdline,function(err,stdout,stderr){
		console.log(stdout);
		callback();
	})
}

