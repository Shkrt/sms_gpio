var pathToScript="/home/pi/.scripts/pinControl.py";

exports.setGPState=function(state,pinNumber,callback){
	var s;
	console.log(state);
	if (state==='off') s=1;
	else if (state='on') s=0;
	var code=s + ':' + pinNumber;
	console.log(code);
	var python = require('child_process').spawn,
		setState=python('python',[pathToScript,'--action',code]);

	setState.stdout.on('data',function(data){
		console.log('stdout:'+data);
		callback(data);
	});

	setState.stderr.on('data',function(data){
		console.log('stderr: '+ data);
	});

	setState.on('close',function(code){
		console.log('child process exited with code' + code);
	})
}

exports.getGPState=function(pinNumber,callback){
	var python = require('child_process').spawn,
		readState=python('python',[pathToScript,'--read',pinNumber]);
	readState.stdout.on('data',function(data){
		console.log('stdout:'+data);
		callback(data);
	})

	readState.stderr.on('data',function(data){
		console.log('stderr: '+data);
	});
	
	readState.on('close',function(code){
		console.log('child process exited with code '+code);
	});
}

exports.getGPValue=function(pinNumber, callback){
	var python = require('child_process').spawn,
		readValue=python('python',[pathToScript,'--value',pinNumber]);

	readValue.stdout.on('data',function(data){
		console.log('stdout:'+data);
		callback(data);
	})

	readValue.stderr.on('data',function(data){
		console.log('stderr: '+data);
	});

	readValue.on('close',function(code){
		console.log('child process exited with code '+code);
	});
}
		  
