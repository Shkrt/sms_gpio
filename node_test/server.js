var express=require('express'),
	http=require("http"),
	app=express(),
	datasource=require("./sqlite.js"),
	cron=require('./cron.js'),
	gpio=require('./rgpio.js');
	
var pathToDB="/home/pi/.scripts/node_test/db/diag.db";
var lastNRecs = "SELECT * from ( select datetime(rec_time,'localtime') as rec_time,\
	message,temperature,id from diagInfo order by id DESC) limit 10";
var bodyParser=require('body-parser');
var urlencodedParser=bodyParser.urlencoded({extended:false})
var keyWord="python /home/pi/.scripts/pinControl.py";

app.use(express.static(__dirname+"/client"));
app.use(bodyParser.json())
http.createServer(app).listen(3000);

app.get("/data.json", function(req,res){
	datasource.performQuery(lastNRecs,pathToDB,function(result){
		res.json(result);
	});
});

app.get("/settings",function(req,res){
	res.redirect("settings.html");
});

app.get("/pin",function(req,res){
	gpio.getGPState(req.query.num,function(result){
		var pinState=result.toString();
		console.log(pinState);
		res.send(pinState);
	});
});

app.get("/val",function(req,res){
	gpio.getGPValue(req.query.num,function(result){
		var pinValue=result.toString();
		console.log(pinValue);
		res.send(pinValue);
	});
});

app.get("/cron",function(req,res){
	cron.getValueFromCrontab(keyWord,function(result){
		res.send(result);
	});
})

app.get("/set",function(req,res){
	gpio.setGPState(req.query.s,req.query.p,function(result){
		var pinState=result.toString();
		console.log(pinState);
		res.send(pinState);		
	});
});

app.post("/editCron",urlencodedParser,function(req,res){
	var val=req.body.inputValue;
	var amount=req.body.selectAmount;
	cron.editCrontab(val,amount,function(){
		res.redirect("/settings");
	})	
});


