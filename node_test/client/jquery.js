var pinNum=14;
var aPinNum=4;
var getPinUrl="/pin?num="+pinNum;
var getValUrl="/val?num="+aPinNum;
var getCronUrl="/cron"

$(document).ready(function(){
	$.getJSON("/data.json",insertIntoLog)
	$.get(getPinUrl,insertCurrentValue)
	$.get(getValUrl,insertIntoTemp)
	$.get(getCronUrl,insertIntoCron)
});

var insertIntoLog=function(data){
	if (data.length===10){
		for (i=0;i<10;i++){
			$('#scrollbox').append
			('<p>'+data[i]['rec_time']+'  '+data[i]['message']+';\
		       	 The temperature is: '+data[i]['temperature'] +'</p>');
		}
	}
};


$(document).on('click', '.btn-success',function(){
	var url="/set?s=off&p="+pinNum;
		$.get(url,function(val){
			insertCurrentValue('1');
})})

$(document).on('click', '.btn-danger',function(){
	var url="/set?s=on&p="+pinNum;
		$.get(url,function(val){
			insertCurrentValue('0');
})})

var insertCurrentValue=function(d){
        var state = 'UNKNOWN';
	var button='';
	var data=d.replace(/\r?\n|\r/g,"");
	if(data==='0'){
		state='ON';
		buttonClass='btn btn-success';
		buttonText='Turn off';
	}
	else if (data==='1'){
		state='OFF';
		buttonClass='btn btn-danger';
		buttonText='Turn on';
	}

	$('#currentState > span').text('Current boiler state is '+state);
	$('#pinControl').attr('class',buttonClass);
	$('#pinControl').text(buttonText);
}

var insertIntoTemp=function(data){
	var temp='UNKNOWN';
	if (data!='NULL'){
		temp=data;
	}

	$('#currentTemp > h1').text(temp);
}

var insertIntoCron=function(data){
	$('#schedule').text('Schedule runs every '+data);
}
