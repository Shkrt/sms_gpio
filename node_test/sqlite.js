var sqlite=require('sqlite3').verbose()

exports.performQuery=function(query,path,callback){
	var db=new sqlite.Database(path);
	db.serialize(function(){
		db.all(query, function(err,rows){
			callback(rows);
		})
	})
	db.close();
};
