var SerialPort = require ("serialport").SerialPort
var portName='/dev/ttyUSB3'
var datasource=require("./sqlite.js")
var dataPath=('/home/pi/.scripts/node_test/db/diag.db')
var inputPin=4;
var outputPin=14;
var state;
var opNumber="+7917xxxxxxx";
var pathToScript="/home/pi/.scripts/pinControl.py";
var log4js = require('log4js'); 
log4js.loadAppender('file');
log4js.addAppender(log4js.appenders.file('/home/pi/.scripts/node_test/smsserv.log'), 'smsserv');
var logger = log4js.getLogger('smsserv');

process.on('SIGINT', function () {
	logger.fatal('Ctrl-C...');
        process.exit();
});

try{
	var serialPort = new SerialPort(portName,{
		baudrate: 115200,
		databits: 8,
		parity: 'none',
		stopBits: 1
	});
}catch(err){
	logger.error(err);
}

serialPort.on("open", function(){
	logger.info("opened serial");
	serialPort.on('data', function(data){
		var dataString=data.toString();
		if (dataString.trim() !==''){
			var arr=dataString.split('\r\n');
			for(var i=0;i<arr.length;i++){
				if(arr[i].indexOf('CMTI')>=0){
					logger.info("mti received")
					var result = parseInput(arr[i]);
					readSMS(result.memType,result.index);
				}
				if(arr[i].indexOf('CMGR')>=0){
					var messageParams=arr[i].split(',');
					var senderNumber="";
					if (messageParams.length>1){
						senderNumber=messageParams[1];
						logger.info("sender number is: "+senderNumber);
					}
					var messageText=arr[i+1].toLowerCase();
					if(messageText=='on'){
						performWork('0:',messageText,senderNumber);
					}
					else if(messageText=='off'){
						performWork('1:',messageText,senderNumber);
					}
					clearSMS();
				}
			}
		}
	})
})


function parseInput(arr){
	var temp=arr.split(':');
	if (temp.length>0){
		var nextSplit = temp[1].split(',');
		if (nextSplit.length>0){
			var result = {
				memType:nextSplit[0].trim(),
				index:nextSplit[1]
			}
			return result;
		}
	}
}

function sendSMS(stateRepr,sendNumber,value){
	msgText='Boiler is now '+stateRepr+'. Temperature is '+value;
	setTimeout(function(){
		serialPort.write('AT+CMGF=1\r');
		setTimeout(function(){
			wr='AT+CMGS='+sendNumber+'\r';
			logger.info(wr);
			serialPort.write(wr);
			setTimeout(function(){
				serialPort.write(msgText);
				serialPort.write('\r');
				setTimeout(function(){
					serialPort.write('\x1A');
				}, 100);
			}, 100);
		}, 100);
	}, 100);
}

function performWork(fArg,msg,sendNumber){
	var args=fArg+outputPin;
	var python=require('child_process').spawn('python',['/home/pi/.scripts/pinControl.py','--action',args]);
	var diagMsg="Control has been set to " + msg
	var data = 'UNDEFINED'
	getGPValue(inputPin,function(result){
		data=result;
		var q="insert into diagInfo (message,temperature) VALUES ("+"\""+diagMsg+"\""+","+data+")";
		logger.info(diagMsg);
		datasource.performQuery(q,dataPath,function(res){
			logger.info('db query performed');
		});
		sendSMS(msg,sendNumber,data);
	})
}

function readSMS(mem,number){
	serialPort.write("AT+CMGF=1\r");
	serialPort.write("AT+CPMS="+mem+"\r");

	setTimeout(function(){
		serialPort.write("AT+CMGR="+number+"\r");
		logger.info("cmgr sent");
		logger.info('waiting...');
	},3000);
}

function deleteSMS(index){
	serialPort.write("AT+CMGD="+index+"\r");
}

function clearSMS(){
	serialPort.write("AT+CMGD=1,4\r");
}

function getGPValue(pinNumber,callback){
	var python = require('child_process').spawn,
		readValue=python('python',[pathToScript,'--value',pinNumber]);
	
	readValue.stdout.on('data',function(data){
		logger.info('stdout:'+data);
		callback(data);
	})
	
	readValue.stderr.on('data',function(data){
		logger.info('stderr: '+data);
	});
	
	readValue.on('close',function(code){
		logger.info('child process exited with code '+code);
	});
}
