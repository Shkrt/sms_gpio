
import sqlite3

class StorageProvider(object):
	def __init__(self,dir):
		self.datadir=dir		

	def store(self,data):
		raise NotImplementedError("This is the abstract method")

	def show(self,count):
		raise NotImplementedError("This is the abstract method")

class Sqlite(StorageProvider):
	def store(self,*data):
		conn=sqlite3.connect(self.datadir)
		c=conn.cursor()
                query=("""insert into diagInfo (message,temperature) VALUES ("{0}",{1})""".format(data[0],data[1]))
		c.execute(query)
		conn.commit()
		conn.close()
	

	def show(self,count):
		lst=[]
		conn=sqlite3.connect(self.datadir)
		c=conn.cursor()
		query="""select * from diagInfo limit {0}""".format(count)
		for row in c.execute(query):
			lst.append(row)	
		conn.close()
		print (lst)

