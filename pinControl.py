import RPi.GPIO as g
import storage
import sys
import getopt

PIN=14
dbfile='/home/pi/.scripts/node_test/db/diag.db'
buses={4:'/sys/bus/w1/devices/28-00000378444c/w1_slave'}

def setup_pin(pin_number):
    g.setmode(g.BCM)
    g.setwarnings(False)
    g.setup(pin_number,g.OUT)

def toggle_pin(pin_number):
    setup_pin(pin_number)
    state=g.input(pin_number)
    try:
        g.output(pin_number,not state)
    except KeyboardInterrupt:
        g.cleanup()
    finally:
        return (not state)

def get_pin_value(pin_number):
    return 'NULL'

def get_w1bus_value(pin_number):
    tempfile=open(buses[int(pin_number)])
    temp_data='NULL'
    text=tempfile.read()
    tempfile.close()
    split=text.split("\n")
    if len(split)>1:
        crc_data=split[0]
        if 'YES' in crc_data:
            temp_data=(float(split[1].split("=")[1])/1000)
    return temp_data

def get_pin_state(str_pin_number):
    pin_number=int(str_pin_number)
    setup_pin(pin_number)
    return g.input(pin_number)

def set_pin_state(str_state,str_pin_number):
    state=int(str_state)
    pin_number=int(str_pin_number)
    setup_pin(pin_number)
    try:
        g.output(pin_number,state)
    except KeyboardInterrupt:
        g.cleanup()
    finally:
        print g.input(pin_number)
        return g.input(pin_number)

#sudo python ~/.scripts/pinControl.py --read 14
#sudo python pinControl.py --action 1:14

if (__name__=="__main__"):
    if len(sys.argv)>1:
        try:
            opts,args=getopt.getopt(sys.argv[1:],'a:r:v',['action=','read=','value='])
        except getopt.GetoptError:
            sys.exit(2)
        for opt,arg in opts:
            if (opt in('a','--action')) and (':' in arg):
                arguments=arg.split(':')
                to_set=arguments[0]
                pin_num=arguments[1]
                pin_state=set_pin_state(to_set,pin_num)
            elif opt in('r','--read'):
                print get_pin_state(arg)
                raise SystemExit
            elif opt in('v','--value'):
                print get_w1bus_value(arg)
                raise SystemExit
            else:
                print 'arguments are invalid'
                sys.exit()
    else:
        pin_state=toggle_pin(PIN)
    temp_value=get_w1bus_value(4)
    st=storage.Sqlite(dbfile)
    state_repr=""
    if (pin_state==g.HIGH):
        state_repr="off"
    else:
        state_repr="on"
    message="Task has been accomplished; current pin state is {0}".format(state_repr)
    st.store(message,temp_value)

